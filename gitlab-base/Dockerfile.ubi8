ARG UBI_IMAGE=registry.access.redhat.com/ubi8/ubi:8.7

FROM ${UBI_IMAGE}

ARG GITLAB_VERSION
ARG DNF_OPTS
ARG GITLAB_USER=git

LABEL source="https://gitlab.com/gitlab-org/build/CNG/-/tree/master/gitlab-base" \
      name="GitLab Base" \
      maintainer="GitLab Distribution Team" \
      vendor="GitLab" \
      version=${GITLAB_VERSION} \
      release=${GITLAB_VERSION} \
      summary="Base container for GitLab application containers." \
      description="Base container for GitLab application containers."

## Corrects CIS Server Level 1 benchmark finding, CCE-80783-4
## Details: http://static.open-scap.org/ssg-guides/ssg-rhel8-guide-standard.html#xccdf_org.ssgproject.content_rule_dir_perms_world_writable_sticky_bits
## RHEL Bug: https://bugzilla.redhat.com/show_bug.cgi?id=2138434
RUN chmod +t /tmp /var/tmp

COPY scripts/ /scripts

RUN dnf ${DNF_OPTS} upgrade --nodocs -yb \
    && dnf ${DNF_OPTS} install --nodocs -yb \
      findutils \
      less \
      procps \
    && dnf clean all \
    && rm -rf /var/cache/dnf \
    && chgrp -R 0 /scripts \
    && chmod -R g=u /scripts

## Hardening: CIS L1 SCAP
COPY hardening/ /hardening
RUN chmod +x /hardening && \
    /hardening/000_rhel_disable_subscription.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_accounts_umask_etc_bashrc.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_accounts_umask_etc_csh_cshrc.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_accounts_umask_etc_login_defs.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_accounts_umask_etc_profile.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_coredump_disable_backtraces.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_coredump_disable_storage.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_disable_users_coredumps.sh && \
    /hardening/xccdf_org.ssgproject.content_rule_use_pam_wheel_for_su.sh && \
    rm -rf /hardening

ENV CONFIG_TEMPLATE_DIRECTORY=/etc
ENV GITLAB_USER=${GITLAB_USER}

ENTRYPOINT ["/scripts/entrypoint.sh"]
