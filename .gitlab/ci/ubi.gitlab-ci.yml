# RedHat UBI
# included when a UBI branch, tag, pipeline

## prepare

gitlab-ubi-builder:
  extends: .build-job-base
  stage: prepare
  script:
    - export DOCKERFILE_EXT='.ubi8'
    - export IMAGE_TAG_EXT='-ubi8'
    - export UBI_BUILD_IMAGE='false'
    - mkdir -p artifacts/images
    - build_if_needed
  needs: []

# Build images
.build-job-base: &build-job-base
  image: "registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ruby_docker:${BUILDER_RUBY_DOCKER}"
  services:
  - docker:${DOCKER_VERSION}-dind
  retry: 1
  before_script:
    # Always compile assets for auto-deploy builds,
    # this is done for auto-deploy builds
    # so that we do not have to wait for the compile assets job
    # in the gitlab-ee pipeline.
    - |
      if [[ $CI_COMMIT_TAG =~ $AUTO_DEPLOY_TAG_REGEX ]]; then
        echo "Setting COMPILE_ASSETS env variable for auto-deploy"
        export COMPILE_ASSETS=true
      fi
    - mkdir -p artifacts/images artifacts/container_versions
    - source build-scripts/build.sh
    - if [[ "${CI_COMMIT_TAG}" == *-ubi8 || "${CI_COMMIT_REF_NAME}" == *-ubi8 ]]; then
    -   export UBI_PIPELINE="true"
    - fi
    - export DOCKERFILE_EXT='.build.ubi8'
    - export IMAGE_TAG_EXT='-build-ubi8'
    - export UBI_BUILD_IMAGE='true'
    - export TARGET_VERSION=$(get_target_version)
    - export CONTAINER_VERSION="${TARGET_VERSION}"
    - BUILD_IMAGE_VERSION="$(get_version gitlab-ubi-builder)"
    - export BUILD_IMAGE="${CI_REGISTRY_IMAGE}/gitlab-ubi-builder:${BUILD_IMAGE_VERSION}-ubi8"
    - docker login -u "$CI_DEPENDENCY_PROXY_USER" -p "$CI_DEPENDENCY_PROXY_PASSWORD" "$CI_DEPENDENCY_PROXY_SERVER"
    - docker login -u gitlab-ci-token -p "$CI_JOB_TOKEN" "$CI_REGISTRY"
    - list_artifacts ubi
  after_script:
    - source build-scripts/build.sh
    - list_artifacts ubi
  artifacts:
    paths:
      - artifacts/
  needs:
    - gitlab-ubi-builder

## prepare phase one

build:cfssl-self-sign:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${CFSSL_VERSION}" | sha1sum))
    - build_if_needed --build-arg "CFSSL_VERSION=$CFSSL_VERSION"
                      --build-arg "CFSSL_CHECKSUM_SHA256=$CFSSL_CHECKSUM_SHA256"
    - copy_assets

build:gitlab-go:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$TARGET_VERSION$GO_VERSION" | sha1sum))
    - build_if_needed --build-arg "GO_VERSION=$GO_VERSION"
    - copy_assets

build:gitlab-gomplate:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${GOMPLATE_VERSION}" | sha1sum))
    - build_if_needed --build-arg "GOMPLATE_VERSION=$GOMPLATE_VERSION"
    - copy_assets

build:gitlab-graphicsmagick:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${GM_VERSION}" | sha1sum))
    - build_if_needed --build-arg "GM_VERSION=$GM_VERSION"
    - copy_assets

build:gitlab-exiftool:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "${TARGET_VERSION}${EXIFTOOL_VERSION}" | sha1sum))
    - build_if_needed --build-arg "EXIFTOOL_VERSION=$EXIFTOOL_VERSION"
    - copy_assets

build:gitlab-ruby:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${RUBY_VERSION}" | sha1sum))
    - build_if_needed --build-arg "RUBY_VERSION=$RUBY_VERSION"
    - copy_assets
    - echo -n "${CONTAINER_VERSION}" > artifacts/container_versions/build_gitlab-ruby_tag.txt

build:gitlab-python:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION$PYTHON_VERSION" | sha1sum))
    - build_if_needed --build-arg "PYTHON_VERSION=${PYTHON_VERSION}"
    - copy_assets

build:postgresql:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${PG_VERSION}" | sha1sum))
    - build_if_needed --build-arg "PG_VERSION=$PG_VERSION"
    - copy_assets

## prepare phase two

build:gitlab-container-registry:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-go
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_CONTAINER_REGISTRY_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "REGISTRY_VERSION=${GITLAB_CONTAINER_REGISTRY_VERSION}"
                      --build-arg "REGISTRY_NAMESPACE=gitlab-org"
    - copy_assets

build:gitaly:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-go
    - build:gitlab-ruby
  script:
    - import_assets artifacts/ubi/{gitlab-go,gitlab-ruby}.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export shell_container=($(echo -n "$BASE_VERSION$go_version$TARGET_VERSION$GITLAB_SHELL_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$shell_container$GITALY_SERVER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "GITALY_SERVER_VERSION=${GITALY_SERVER_VERSION}"
                      --build-arg "GITALY_GIT_REPO_URL=${GITALY_GIT_REPO_URL}"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets

build:gitlab-elasticsearch-indexer:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-go
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_ELASTICSEARCH_INDEXER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "VERSION=${GITLAB_ELASTICSEARCH_INDEXER_VERSION}"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets

build:gitlab-metrics-exporter:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-go
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_METRICS_EXPORTER_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "VERSION=${GITLAB_METRICS_EXPORTER_VERSION}"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets

build:gitlab-pages:
  extends: .build-job-base
  stage: prepare:phase-two
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION$GITLAB_PAGES_VERSION$go_version$CI_PIPELINE_CREATED_AT" | sha1sum))
    - build_if_needed --build-arg "VERSION=${GITLAB_PAGES_VERSION}"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets
  needs:
    - build:gitlab-go

build:gitlab-logger:
  extends: .build-job-base
  stage: prepare:phase-two
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$go_version$TARGET_VERSION$GITLAB_LOGGER_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - build_if_needed --build-arg "GITLAB_LOGGER_VERSION=$GITLAB_LOGGER_VERSION"
                      --build-arg "GITLAB_LOGGER_SHA256=${GITLAB_LOGGER_SHA256}"
    - copy_assets
    - echo -n "${TARGET_VERSION}" > artifacts/container_versions/build_gitlab-logger_tag.txt
  needs:
    - build:gitlab-go

build:kubectl:
  extends: .build-job-base
  stage: prepare:phase-one
  script:
    - export CONTAINER_VERSION=($(echo -n "$TARGET_VERSION${KUBECTL_VERSION}${YQ_VERSION}" | sha1sum))
    - build_if_needed --build-arg "KUBECTL_VERSION=${KUBECTL_VERSION}"
                      --build-arg "YQ_VERSION=${YQ_VERSION}"
    - copy_assets

build:gitlab-webservice-ee:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-python
    - build:gitlab-ruby
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - rails_container=($(echo -n "$ruby_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - build_if_needed
    - copy_assets

build:gitlab-sidekiq-ee:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-python
    - build:gitlab-ruby
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - rails_container=($(echo -n "$ruby_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - build_if_needed
    - copy_assets

build:gitlab-exporter:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-ruby
    - build:postgresql
  script:
    - import_assets artifacts/ubi/{gitlab-ruby,postgresql}.tar.gz
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - ruby_container=($(echo -n "$ruby_version$GITLAB_EXPORTER_VERSION" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$ruby_container$TARGET_VERSION" | sha1sum))
    - build_if_needed
    - copy_assets

build:gitlab-mailroom:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-ruby
  script:
    - import_assets artifacts/ubi/gitlab-ruby.tar.gz
    - export CONTAINER_VERSION=($(echo -n "$ruby_version$MAILROOM_VERSION" | sha1sum))
    - build_if_needed --build-arg "MAILROOM_VERSION=$MAILROOM_VERSION"
    - copy_assets

build:gitlab-kas:
  extends: .build-job-base
  stage: prepare:phase-two
  needs:
    - build:gitlab-go
  script:
    - import_assets artifacts/ubi/gitlab-go.tar.gz
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export TARGET_VERSION=$(get_target_version)
    - export CONTAINER_VERSION=($(echo -n "$GITLAB_KAS_VERSION$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "GITLAB_NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "GITLAB_KAS_VERSION=${GITLAB_KAS_VERSION}"
                      --build-arg "FETCH_ARTIFACTS_PAT=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "CI_API_V4_URL=${CI_API_V4_URL}"
    - copy_assets

## prepare phase three

build:gitlab-rails-ee:
  extends: .build-job-base
  stage: prepare:phase-three
  needs:
    - build:gitlab-ruby
    - build:postgresql
    - build:gitlab-elasticsearch-indexer
    - build:gitlab-metrics-exporter
    - build:gitlab-graphicsmagick
    - build:gitlab-exiftool
  script:
    - import_assets artifacts/ubi/{gitlab-ruby,postgresql,gitlab-elasticsearch-indexer,gitlab-metrics-exporter,gitlab-graphicsmagick,gitlab-exiftool}.tar.gz
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$ruby_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export ASSETS_IMAGE="${ASSETS_IMAGE_REGISTRY_PREFIX}/${EE_PROJECT}/${ASSETS_IMAGE_PREFIX}-ee:${GITLAB_ASSETS_TAG}"
    - fetch_assets
    - build_if_needed --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "PROJECT=${EE_PROJECT}"
                      --build-arg "VERSION=${GITLAB_VERSION}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
                      --build-arg "ASSETS_IMAGE=${ASSETS_IMAGE}"
    - copy_assets

build:gitlab-toolbox-ee:
  extends: .build-job-base
  stage: prepare:phase-three
  needs:
    - build:gitlab-python
    - build:gitaly
    - build:gitlab-ruby
  script:
    - import_assets artifacts/ubi/gitlab-python.tar.gz
    - import_assets artifacts/ubi/gitaly.tar.gz
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - rails_container=($(echo -n "$ruby_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$rails_container$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "S3CMD_VERSION=$S3CMD_VERSION"
    - copy_assets

build:gitlab-shell:
  extends: .build-job-base
  stage: prepare:phase-three
  needs:
    - build:gitlab-go
    - build:gitlab-ruby
    - build:gitlab-logger
    - build:gitlab-gomplate
  script:
    - import_assets artifacts/ubi/{gitlab-go,gitlab-ruby}.tar.gz
    - go_dir_version=$(get_version gitlab-go)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$go_version$TARGET_VERSION$GITLAB_SHELL_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - build_if_needed --build-arg "VERSION=${GITLAB_SHELL_VERSION}"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets

## prepare phase four

build:gitlab-workhorse-ee:
  extends: .build-job-base
  stage: prepare:phase-four
  needs:
    - build:gitlab-go
    - build:gitlab-exiftool
    - build:gitlab-rails-ee
  script:
    - import_assets artifacts/ubi/{gitlab-go,gitlab-rails-ee,gitlab-exiftool}.tar.gz
    - rails_version=$(get_version gitlab-rails)
    - ruby_version=$(cat artifacts/container_versions/build_gitlab-ruby_tag.txt)
    - go_dir_version=$(get_version gitlab-go)
    - go_version=$(cat artifacts/container_versions/gitlab-go_tag.txt)
    - rails_container=($(echo -n "$ruby_version$TARGET_VERSION$GITLAB_VERSION$CI_PIPELINE_CREATED_AT" | sha1sum))
    - export CONTAINER_VERSION=($(echo -n "$BASE_VERSION$rails_container$go_version$TARGET_VERSION" | sha1sum))
    - build_if_needed --build-arg "VERSION=$GITLAB_VERSION"
                      --build-arg "NAMESPACE=${GITLAB_NAMESPACE}"
                      --build-arg "API_URL=${CI_API_V4_URL}"
                      --build-arg "API_TOKEN=${FETCH_DEV_ARTIFACTS_PAT}"
    - copy_assets

# Final images
## phase zero
ubi-stable:
  extends: .job-base
  stage: phase-zero
  script:
    # UBI image can not be used with DEPENDENCY_PROXY
    - record_stable_image "${UBI_IMAGE}"

gitlab-base:
  extends: .gitlab-base
  needs:
    - ubi-stable

## phase one
alpine-certificates:
  extends: .alpine-certificates
  needs:
    - ubi-stable

cfssl-self-sign:
  extends: .cfssl-self-sign
  needs:
    - build:cfssl-self-sign
    - ubi-stable

kubectl:
  extends: .kubectl
  needs:
    - ubi-stable
    - build:kubectl

gitlab-ruby:
  extends: .gitlab-ruby
  needs:
    - gitlab-base
    - build:gitlab-ruby

## phase two

gitlab-exporter:
  extends: .gitlab-exporter
  needs:
    - gitlab-ruby
    - build:gitlab-exporter

gitlab-mailroom:
  extends: .gitlab-mailroom
  needs:
    - gitlab-ruby
    - build:gitlab-mailroom

## phase three

# NOTE: UBI does not have `gitlab-logger`
# these come directly from build:*

## phase four

gitlab-container-registry:
  extends: .gitlab-container-registry
  needs:
    - ubi-stable
    - build:gitlab-container-registry
    - build:gitlab-gomplate

gitlab-kas:
  extends: .gitlab-kas
  needs:
    - build:gitlab-kas
    - ubi-stable

gitlab-pages:
  extends: .gitlab-pages
  needs:
    - gitlab-base
    - build:gitlab-gomplate
    - build:gitlab-pages

gitlab-shell:
  extends: .gitlab-shell
  needs:
    - gitlab-base
    - build:gitlab-logger
    - build:gitlab-gomplate
    - build:gitlab-shell

## phase five

gitaly:
  extends: .gitaly
  needs:
    - build:gitaly
    - build:gitlab-logger
    - gitlab-ruby

gitlab-rails-ee:
  extends: .gitlab-rails-ee
  needs:
    - gitlab-ruby
    - build:gitlab-rails-ee
    - build:postgresql
    - build:gitlab-graphicsmagick
    - build:gitlab-exiftool
    - build:gitlab-elasticsearch-indexer
    - build:gitlab-gomplate

## phase six

gitlab-geo-logcursor:
  extends: .gitlab-geo-logcursor
  needs:
    - gitlab-rails-ee

gitlab-sidekiq-ee:
  extends: .gitlab-sidekiq-ee
  needs:
    - gitlab-rails-ee
    - build:gitlab-sidekiq-ee
    - build:gitlab-python
    - build:gitlab-logger

gitlab-toolbox-ee:
  extends: .gitlab-toolbox-ee
  needs:
    - gitlab-rails-ee
    - build:gitlab-python
    - build:gitaly
    - build:gitlab-toolbox-ee

gitlab-webservice-ee:
  extends: .gitlab-webservice-ee
  needs:
    - gitlab-rails-ee
    - build:gitlab-webservice-ee
    - build:gitlab-python
    - build:gitlab-logger

gitlab-workhorse-ee:
  extends: .gitlab-workhorse-ee
  needs:
    - gitlab-rails-ee
    - build:gitlab-exiftool
    - build:gitlab-gomplate
    - build:gitlab-workhorse-ee

## Release handling

ubi-assets-release:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ubi-release
  stage: release
  needs:
    - build:kubectl
    - build:postgresql
    - build:gitlab-python
    - build:gitlab-ruby
    - build:gitlab-go
    - build:gitlab-logger
    - build:gitlab-gomplate
    - build:gitlab-exiftool
    - build:gitlab-pages
    - build:gitlab-container-registry
    - build:gitlab-mailroom
    - build:gitlab-exporter
    - build:gitlab-shell
    - build:gitaly
    - build:gitlab-elasticsearch-indexer
    - build:gitlab-workhorse-ee
    - build:gitlab-rails-ee
    - build:gitlab-toolbox-ee
    - build:gitlab-webservice-ee
    - build:gitlab-sidekiq-ee
    - build:gitlab-kas
  rules:
    - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+\.\d+(-rc\d+)?-ubi8$/ && $CI_PROJECT_PATH == "gitlab/charts/components/images"'
      when: manual
  before_script:
    - export UBI_RELEASE_TAG=${CI_COMMIT_TAG:-master}
    - export AWS_ACCESS_KEY_ID="${GPG_KEY_AWS_ACCESS_KEY_ID}"
    - export AWS_SECRET_ACCESS_KEY="${GPG_KEY_AWS_SECRET_ACCESS_KEY}"
    - aws s3 cp --quiet ${GPG_KEY_LOCATION} /tmp/private.pem
    - gpg --batch --import /tmp/private.pem
    - rm /tmp/private.pem
  script:
    - export AWS_ACCESS_KEY_ID="${UBI_ASSETS_AWS_ACCESS_KEY_ID}"
    - export AWS_SECRET_ACCESS_KEY="${UBI_ASSETS_AWS_SECRET_ACCESS_KEY}"
    - bash build-scripts/ubi-offline/assets-release.sh
    - rm -rf artifacts/*

.preflight-certification:
  image: ${PREFLIGHT_IMAGE_REGISTRY_PREFIX}/${PREFLIGHT_IMAGE_PREFIX}/preflight:1.2.1
  extends:
    - .redhat_certification # rules
  stage: container-scanning
  retry: 1
  allow_failure: true
  artifacts:
    paths:
      - reports/*
  dependencies: []
  before_script:
    - podman login -u ${CI_REGISTRY_USER} -p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}
    - '[[ -d reports ]] || mkdir reports'
  script:
    - 'echo "Scanning image: ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_NAME}"'
    - '[[ -z "${PROJECT_ID}" ]] && PROJECT_ID=$(yq .${IMAGE_NAME}.pid redhat-projects.yaml)'
    - preflight check container --pyxis-api-token=${REDHAT_API_TOKEN} --docker-config /run/containers/0/auth.json --certification-project-id ${PROJECT_ID} --submit ${CI_REGISTRY_IMAGE}/${IMAGE_NAME}:${CI_COMMIT_REF_NAME} > reports/${IMAGE_NAME}-cert.yaml
    - test $(jq .passed reports/${IMAGE_NAME}-cert.yaml) == 'true'

gitlab-sidekiq-ee Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-sidekiq-ee
  needs:
    - gitlab-sidekiq-ee

gitlab-toolbox-ee Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-toolbox-ee
  needs:
    - gitlab-toolbox-ee

gitlab-webservice-ee Certfication:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-webservice-ee
  needs:
    - gitlab-webservice-ee

gitlab-workhorse-ee Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-workhorse-ee
  needs:
    - gitlab-workhorse-ee

gitlab-mailroom Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-mailroom
  needs:
    - gitlab-mailroom

gitlab-shell Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-shell
  needs:
    - gitlab-shell

gitlab-exporter Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-exporter
  needs:
    - gitlab-exporter

gitlab-container-registry Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-container-registry
  needs:
    - gitlab-container-registry

gitaly Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitaly
  needs:
    - gitaly

alpine-certificates Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: alpine-certificates
  needs:
    - alpine-certificates

cfssl-self-sign Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: cfssl-self-sign
  needs:
    - cfssl-self-sign

kubectl Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: kubectl
  needs:
    - kubectl

gitlab-kas Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-kas
  needs:
    - gitlab-kas

gitlab-pages Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-pages
  needs:
    - gitlab-pages

gitlab-geo-logcursor Certification:
  extends: .preflight-certification
  variables:
    IMAGE_NAME: gitlab-geo-logcursor
  needs:
    - gitlab-geo-logcursor

## UBI is only released for EE.
# Skip all CE jobs, used to mock definitions for `final-images-listing`
.ubi-skipped:
  stage: phase-zero
  script:
    - echo "NOOP"; exit 1
  rules:
    - when: never

gitlab-rails-ce:
  extends: .ubi-skipped

gitlab-sidekiq-ce:
  extends: .ubi-skipped

gitlab-toolbox-ce:
  extends: .ubi-skipped

gitlab-webservice-ce:
  extends: .ubi-skipped

gitlab-workhorse-ce:
  extends: .ubi-skipped

