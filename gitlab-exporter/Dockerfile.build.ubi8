ARG BUILD_IMAGE=

FROM ${BUILD_IMAGE}

ARG GITLAB_EXPORTER_VERSION=12.1.1
ENV LIBDIR ${LIBDIR:-"/usr/lib64"}

ADD gitlab-ruby.tar.gz /
ADD postgresql.tar.gz /
ADD https://gitlab.com/gitlab-org/gitlab-exporter/-/raw/master/LICENSE /licenses/GitLab.txt

COPY shared/build-scripts/ /build-scripts

RUN install /usr/local/postgresql/bin/* /usr/bin/ \
    && cp -R /usr/local/postgresql/lib/. ${LIBDIR}/ \
    && mv /usr/local/postgresql/include/* /usr/include/ \
    && mv /usr/local/postgresql/share/* /usr/share/ \
    && gem install gitlab-exporter -v ${GITLAB_EXPORTER_VERSION} \
    && /build-scripts/cleanup-gems ${LIBDIR}/ruby/gems

RUN mkdir -p /assets /assets${LIBDIR} \
    && cp -R /usr/local/postgresql/lib/. /assets${LIBDIR}/ \
    && cp -R --parents \
        ${LIBDIR}/ruby/gems \
        /usr/bin/gitlab-exporter \
        /licenses \
        /assets
